//
//  AddDeviceViewController.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/19/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import UIKit
import RealmSwift

class AddDeviceViewController: UIViewController {
    
     // MARK: - View Component Variables
    @IBOutlet weak var deviceName: UITextField!
    
    @IBOutlet weak var os: UITextField!

    @IBOutlet weak var manufacturer: UITextField!
    
     // MARK: - View Component Actions
    @IBAction func cancel(_ sender: UIBarButtonItem) {
                self.navigationController!.popToRootViewController(animated: false)
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {

        //Verify all the fields
        guard let name = deviceName.text, name !=  "" else {
            //Inform user about what is missing
            AlertHelp.showAlert(title:  NSLocalizedString("titleAlert", comment: "title"), message:  NSLocalizedString("deviceNameValidationMessage", comment: "device"))
            return
        }
        
        guard let osText = os.text, osText !=  "" else {
            AlertHelp.showAlert(title:  NSLocalizedString("titleAlert", comment: "title"), message:  NSLocalizedString("osValidationMessage", comment: "os"))
            return
        }
        
        guard let manufacturerText = manufacturer.text, manufacturerText != "" else {
            AlertHelp.showAlert(title:  NSLocalizedString("titleAlert", comment: "title"), message:  NSLocalizedString("manufacturerValidationMessage", comment: "manufacturer"))
            return
        }
        
        
        let parameters: [String: Any] = [
            "device" : self.deviceName.text!,
            "os" : self.manufacturer.text!,
            "manufacturer" : self.os.text!
        ]

        let  url  = Constants.BaseURLs.baseUrl + Constants.DeviceURLs.deviceEndpoint
        var isSync = false
        
        //Adding to Data base
        if OfflineServiceService.isOnlineStatus() {
            NetworkRequestService.postDevice(url: url, parameters: parameters) {
                (result: Any) in
                print("device \(String(describing: result))")
                isSync = true
            }
        }
        
        //store it in our local database
        let deviceObj = DeviceModel()
        deviceObj.device = self.deviceName.text!
        deviceObj.manufacturer = self.manufacturer.text!
        deviceObj.os = self.os.text!
        deviceObj.createdOn = Date()
        deviceObj.isCheckedOut = false
        deviceObj.id = UUID().hashValue
        deviceObj.isSync = isSync
        deviceObj.isRemoteDevice = false
        
        // Get the  Realm
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(deviceObj)
        }
        
        let cancelAlert = UIAlertController(title: NSLocalizedString("titleAlert", comment: "title") , message: NSLocalizedString("registeredDeviceMessage", comment: "registered")  , preferredStyle: UIAlertControllerStyle.alert)
        
        cancelAlert.addAction(UIAlertAction(title:  NSLocalizedString("alertOk", comment: "Ok") , style: .default, handler: { (action: UIAlertAction!) in
            //do nothing
            _ = self.navigationController?.popViewController(animated: true)
        }))
        
        present(cancelAlert, animated: true, completion: nil)

    }
    
    // MARK: - UIViewController Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: NSLocalizedString("alertCancel", comment: "cancel") , style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddDeviceViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        // Do any additional setup after loading the view.
        deviceName.placeholder = NSLocalizedString("deviceLabel", comment: "device")
        os.placeholder = NSLocalizedString("osLabel", comment: "os")
        manufacturer.placeholder = NSLocalizedString("manufacturerLabel", comment: "manufacturer")
        
        deviceName.textAlignment =  .center
        os.textAlignment =  .center
        manufacturer.textAlignment =  .center
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - AddDeviceViewController

    func back(sender: UIBarButtonItem) {
        
        guard deviceName.text!.isEmpty, os.text!.isEmpty, manufacturer.text!.isEmpty else {
            //Inform user about what is missing
            let cancelAlert = UIAlertController(title: NSLocalizedString("titleAlert", comment: "title") , message: NSLocalizedString("cancelAddNewDeviceMessage", comment: "cancel")  , preferredStyle: UIAlertControllerStyle.alert)
            
            cancelAlert.addAction(UIAlertAction(title:  NSLocalizedString("alertStop", comment: "Stop") , style: .default, handler: { (action: UIAlertAction!) in
                //do nothing
            }))
            
            cancelAlert.addAction(UIAlertAction(title: NSLocalizedString("alertCancel", comment: "Cancel") , style: .destructive, handler: { (action: UIAlertAction!) in
                //do nothing
                // Go back to the previous ViewController
                _ = self.navigationController?.popViewController(animated: true)
            }))
            
            present(cancelAlert, animated: true, completion: nil)
            
            return

        }
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }

}
