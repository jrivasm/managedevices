//
//  DateConversion.swift
//  ManageDevices
//
//  Created by Janet Rivas on 5/6/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//


import Foundation
import UIKit


class TimestampHelper
{
    static func getCurrentTimestamp(timestamp : Date) -> String {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.ReferenceType.system
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let st = formatter.string(from: timestamp)
        return st
        
    }


}
