//
//  Result.swift
//  ManageDevices
//
//  Created by Janet Rivas on 5/7/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import UIKit
import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}
