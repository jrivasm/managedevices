//
//  OfflineService.swift
//  ManageDevices
//
//  Created by Janet Rivas on 5/7/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import Foundation
import SystemConfiguration
import RealmSwift

class OfflineServiceService {

    static func isOnlineStatus() -> Bool {
        
        // Socket address,
        var zeroSocketAddress = sockaddr_in()
        
        //Socket address, internet style
        zeroSocketAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroSocketAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroSocketAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            //There is not  defaul  router Reachability
            return false
        }
        
        /*SCNetworkReachabilityFlags  Flags that indicate whether the specified network
         nodename or address is reachable, whether a connection is
         required, and whether some user intervention may be required
         when establishing a connection*/
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            //is not connecte to network
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    

    
}
