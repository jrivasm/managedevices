//
//  NetworkRequetAlamonfire.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/23/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import Alamofire
import UIKit
import Foundation


class NetworkRequestService
{
    
    static func requestDevicesList( url: String, completionHandler: @escaping (_ response: [AnyObject]? ) -> Void) {
        
        Alamofire.request(url).responseJSON { response in
            debugPrint(response)
            completionHandler((response.result.value as? [AnyObject]))
            return
        }
        
    }
    
    static func postDevice( url: String, parameters : [String: Any], completionHandler: @escaping (_ response: Any ) -> Void) {
        
        Alamofire.request(url,  method:.post, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
            .responseJSON { response in
                debugPrint(response)
                completionHandler((response.result.value as Any))
                return

        }
        
    }

}
