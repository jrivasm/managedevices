//
//  databaseHelper.swift
//  ManageDevices
//
//  Created by Janet Rivas on 5/6/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class DatabaseHelperService
{
    
    static func getAllDBObjects() -> [DeviceModel]? {
        
        let realm = try! Realm()
        let objects = realm.objects(DeviceModel.self)
        let unsyncDevices = objects.toArray(ofType : DeviceModel.self) as [DeviceModel]
        return unsyncDevices.count > 0 ? unsyncDevices : nil
        
    }

    //Update checkout
    static func updateObject(updateObjet: DeviceModel, userName : String, timestamp : Date, isSync: Bool) {
        
        // Get the default Realm
        let realm = try! Realm()
        
        try? realm.write {
            updateObjet.lastCheckedOutBy = userName
            updateObjet.lastCheckedOutDate = timestamp
            updateObjet.isSync = isSync
            updateObjet.isCheckedOut = true
            realm.add(updateObjet, update: true)
        }
        
    }
    
    
    // get devices from a query and return them in [DeviceModel] arrays
    static func getDevicesFromPredicate(query : NSPredicate ) -> [DeviceModel]? {
        
        let realm = try! Realm()
        let objects = realm.objects(DeviceModel.self).filter(query)
        let unsyncDevices = objects.toArray(ofType : DeviceModel.self) as [DeviceModel]
        
        return unsyncDevices.count > 0 ? unsyncDevices : nil
     
    }
    
    static func deleteAllDBObjects() {
        
        // Get the default Realm
        let realm = try! Realm()
        realm.beginWrite()
        
        // query all objects where the id in not included
        let objectsToDelete = realm.objects(DeviceModel.self).filter("isRemoteDevice =  true")
        
        // and then just remove the set with
        realm.delete(objectsToDelete)
        try! realm.commitWrite()

    }

    
}
