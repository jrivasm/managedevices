//
//  DevicesCast.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/27/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class DevicesObjCast {
    
    static func castDeviceFromNSArray( devicesArray : [AnyObject]?, isRemoteDevice : Bool, isSync : Bool)-> [DeviceModel] {
        
        //Create a device object
        var devicesArrayObj = [DeviceModel]()
        
        guard let deviceCount = devicesArray?.count  else {
            return devicesArrayObj
        }
        print("\(deviceCount) device(s).")
        
        devicesArrayObj.removeAll()
        devicesArrayObj = [DeviceModel]()
        
        
        for device in devicesArray! {
            print(device)
            let deviceObj = DeviceModel()
            
            deviceObj.device = device["device"] as! String
            deviceObj.id =  device["id"] as! Int
            deviceObj.manufacturer = device["manufacturer"] as! String
            deviceObj.os = device["os"] as! String
            deviceObj.isCheckedOut =  device["isCheckedOut"] as! Bool
            deviceObj.isRemoteDevice = isRemoteDevice
            deviceObj.isSync = isSync
            
            //optional values
            deviceObj.lastCheckedOutDate = device["lastCheckedOutDate"] as? Date
            deviceObj.lastCheckedOutBy = device["lastCheckedOutBy"] as? String
            
            devicesArrayObj.append(deviceObj)

        }
        
        return devicesArrayObj
    }
}
