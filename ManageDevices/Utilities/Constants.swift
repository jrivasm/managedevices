//
//  Constants.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/23/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import Foundation
import UIKit


struct Constants {
    struct BaseURLs {
        static let  baseUrl = "http://private-1cc0f-devicecheckout.apiary-mock.com"
    }
    
    struct DeviceURLs {
        static let  deviceEndpoint = "/devices"
    }
}
