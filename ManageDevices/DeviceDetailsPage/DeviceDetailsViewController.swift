//
//  DeviceDetailsViewController.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/19/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import UIKit

class DeviceDetailsViewController: UIViewController, AlertViewControllerDelegate {
    
    var dataDetails :DeviceModel!
    
    //MARK interface builder variables
    @IBOutlet weak var checkOutButton: UIButton!
    
    @IBOutlet weak var deviceLabel: UILabel!
    
    @IBOutlet weak var osLabel: UILabel!
    
    @IBOutlet weak var manufacturerLabel: UILabel!
    
    @IBOutlet weak var checkoutLabel: UILabel!
    
    @IBAction func checkoutAction(_ sender: Any) {
        
        let alert = AlertViewController.sharedInstance
        alert.delegate = self
        alert.SubmitAlertView(viewController: self,title: NSLocalizedString("titleAlert", comment: "title") , message: NSLocalizedString("checkInDeviceMessage", comment: "checkIn"))
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        deviceLabel.text = "\(NSLocalizedString("deviceLabel", comment: "device")) : \(dataDetails.device)"
        osLabel.text = "\(NSLocalizedString("osLabel", comment: "os")) : \(dataDetails.os)"
        manufacturerLabel.text =  "\(NSLocalizedString("manufacturerLabel", comment: "manufacturer")) : \(dataDetails.manufacturer)"
        
        let checkoutBy = dataDetails.lastCheckedOutBy
        print("\(String(describing: dataDetails))")
        
        if dataDetails.isCheckedOut {
             checkoutLabel.text = "Last Check Out on \(String(describing: TimestampHelper.getCurrentTimestamp(timestamp: dataDetails.createdOn))) by \(checkoutBy ?? "no one")"
             checkOutButton.setTitle("Check In", for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    // MARK: - Navigation
    func SubmitAlertViewResult(userName : String) {
        let timestamp = Date()
        var isSync = false
        
        checkoutLabel.text = "Last Check Out on \(String(describing: TimestampHelper.getCurrentTimestamp(timestamp: timestamp))) by \(userName)"
        checkOutButton.setTitle("Check In", for: .normal)
        
        let parameters: [String: Any] = [
            "lastCheckedOutDate": TimestampHelper.getCurrentTimestamp(timestamp: timestamp),
            "lastCheckedOutBy": userName,
            "isCheckedOut": true
        ]
    
        if OfflineServiceService.isOnlineStatus() {
            //Update to the mock API
            let url  = "\(Constants.BaseURLs.baseUrl)\(Constants.DeviceURLs.deviceEndpoint)/\(dataDetails.id)"
                
            NetworkRequestService.postDevice(url: url, parameters: parameters) {
                (result: Any) in
                isSync = true
                //Updating local database
                DatabaseHelperService.updateObject(updateObjet: self.dataDetails, userName : userName, timestamp : timestamp, isSync : isSync)
            }
            
        } else {
            
            //Updating local database
            DatabaseHelperService.updateObject(updateObjet: dataDetails, userName : userName, timestamp : timestamp, isSync : isSync)
        }
        
    }
}
