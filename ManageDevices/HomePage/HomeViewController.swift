//
//  HomeViewController.swift
//  ManageDevices
//
//  Created by Janet Rivas on 4/19/17.
//  Copyright © 2017 Janet Rivas. All rights reserved.
//

import UIKit
import RealmSwift
import ReachabilitySwift
import Realm



class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
   //MARK: -Home page Local Variables
    var deviceListData = [DeviceModel]()
    let cellReuseIdentifier = "cell"
    let reachability = Reachability()!

    @IBOutlet var deviceTableView: UITableView!
    
    
    @IBAction func addDevice(_ sender: UIButton) {
         self.performSegue(withIdentifier: "addDevices", sender: sender)
    }
    
    
    
    @IBAction func reset(_ sender: Any) {
        //This option reset items from API rest (nice to have)
        //Delete remote devices objects in database
        DatabaseHelperService.deleteAllDBObjects()
        retriveRemoteDevices()

    }
    
    
   
    
    //MARK: -Home Page Delegates
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        if OfflineServiceService.isOnlineStatus() {
            retriveAndReloadGeneralDevicesTableView()
        } else {
            //inform the user is offline, but the store a new device is vailable
            AlertHelp.showAlert(title:  NSLocalizedString("titleAlert", comment: "title"), message:  NSLocalizedString("offlineMessage", comment: "offline"))
            retriveAndReloadLocalDevicesTableView()
        }
        
        self.deviceTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        deviceTableView.delegate = self
        deviceTableView.dataSource = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
        retriveAndReloalDevicesTableView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Tableview delegates
    
    //Numbers of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.deviceListData.count > 0){
             return self.deviceListData.count
        } else {
            return 0
        }
    }
    
    //Build cells for table
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "deviceCellView") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "deviceCellView")
        
        let json = self.deviceListData[indexPath.row]
        let obj = json as DeviceModel
        
        cell.textLabel?.text = ("\(String(describing: obj["device"] ?? "")) - \(String(describing: obj["os"] ?? ""))")
        
        if obj["lastCheckedOutBy"] != nil {
            cell.detailTextLabel?.text = ("Check Out by  \(String(describing: obj["lastCheckedOutBy"]  ?? ""))")
        } else {
            cell.detailTextLabel?.text = "Available"
        }
        
        
        return cell
        
    }
    
    //Numbers of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        self.performSegue(withIdentifier: "devicesDetails", sender: self.deviceListData[indexPath.row])
        
    }
    

    //Number of sections in table
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
       
    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let removeAlert = UIAlertController(title: NSLocalizedString("titleAlert", comment: "title") , message: NSLocalizedString("removeDeviceMessage", comment: "remove")  , preferredStyle: UIAlertControllerStyle.alert)
            
                removeAlert.addAction(UIAlertAction(title:  NSLocalizedString("alertDelete", comment: "Delete") , style: .destructive, handler: { (action: UIAlertAction!) in
            
                    //delete from database if that exist
                    let realm = try! Realm()
                    
                    // Query Realm for Device
                    let device = realm.objects(DeviceModel.self).filter("device = '\(self.deviceListData[indexPath.row].device)'")
                    print("devices count : \(device)")
                    
                    // Get the  Realm
                    try! realm.write {
                        // remove from realm
                        realm.delete(device)
                    }
                    
                    // remove the item from the data model
                    self.deviceListData.remove(at: indexPath.row)
                    
                    // delete the table view row
                    tableView.deleteRows(at: [indexPath], with: .fade)

            }))
            
            removeAlert.addAction(UIAlertAction(title: NSLocalizedString("alertCancel", comment: "Cancel") , style: .cancel, handler: { (action: UIAlertAction!) in
                //don't do nothing
            }))
            
            present(removeAlert, animated: true, completion: nil)
            
        }
    }
    
    
    //MARK: - Home help functions
    
    //Numbers of rows in table view
    func retriveAndReloadGeneralDevicesTableView() {
        if UIApplication.isFirstLaunch(){
            retriveRemoteDevices()
        } else {
            retriveAndReloalDevicesTableView()
        }
    }
    
    func retriveRemoteDevices() {
        
        let  url  = Constants.BaseURLs.baseUrl + Constants.DeviceURLs.deviceEndpoint
        NetworkRequestService.requestDevicesList(url: url) {
            (result: [AnyObject]?) in
            
            //Clean deviceList Object Array
            self.deviceListData.removeAll()
            //Casting remote devices DeviceModel object
            
            self.deviceListData = DevicesObjCast.castDeviceFromNSArray(devicesArray: result, isRemoteDevice: true, isSync: true)
            // Get the default Realm
            let realm = try! Realm()
            
            //add remote devices in order to delete them locally
            try! realm.write {
                realm.add(self.deviceListData)
            }
            //And then local
            self.retriveAndReloadLocalDevicesTableView()
        }

    }
    
    //Retriving Local Database that are not comming from rest API
    func retriveAndReloadLocalDevicesTableView() {
        
        // Get the default Realm
        let predicate = NSPredicate(format: "isRemoteDevice == false")
        
        var localDevices = [DeviceModel]()
        
        localDevices = DatabaseHelperService.getDevicesFromPredicate(query : predicate) ?? localDevices
        
        //Add database objects to device list
        self.deviceListData.append(contentsOf: localDevices)
        print("device \(String(describing: self.deviceListData)) device Count \(self.deviceListData.count)")
        
        
        self.deviceTableView.reloadData()
    }
    
    //Retriving Local Database getting back to the page
    func retriveAndReloalDevicesTableView() {
        
        var localDevices = [DeviceModel]()
        localDevices = DatabaseHelperService.getAllDBObjects() ?? localDevices
        
        self.deviceListData = localDevices
        
        print("device Count \( self.deviceListData.count)")
        
        self.deviceTableView.reloadData()
    }
    
    @objc private func reachabilityChanged( notification: NSNotification )
    {
        guard let reachability = notification.object as? Reachability else
        {
            return
        }
        //The sync is better doing when WIFI is available however for prototype poposes it'll be available also for cellular data
        if reachability.isReachable
        {
            print("Network is Reachable")
            //look in database for all no syc devices
            
            //Sync new devices that are not extist in remote database
            let predicate = NSPredicate(format: "isSync == false AND isRemoteDevice == false ")
            //let devices = DatabaseHelperService.getDevicesFromPredicate(query : predicate)!
        
            let realm = try! Realm()
            let objects = realm.objects(DeviceModel.self).filter(predicate)
            let devicesRef = ThreadSafeReference(to: objects)
            
            //Call to GCD  background to sync
            DispatchQueue(label: "com.challenge.ManageDevices", qos: .background).async {
                autoreleasepool {
                let realm = try! Realm()
                guard let devices = realm.resolve(devicesRef) else {
                    return // person was deleted
                }
                for device in devices{
                    print(device)
                    //send to Sycronization for devices are not created on backend
                    let  url  = Constants.BaseURLs.baseUrl + Constants.DeviceURLs.deviceEndpoint
                    let parameters: [String: Any] = [
                        "device" : device.device,
                        "os" : device.manufacturer,
                        "manufacturer" : device.os
                    ]
                    
                    NetworkRequestService.postDevice(url: url, parameters: parameters) {
                        (result: Any) in
                        print("device \(String(describing: result))")
                        //TODOupdate the item as Sync
                        //let isSync = true
                        //DatabaseHelperService.getDevicesFromPredicate(query : predicate)
                    }
                    
                    //if the device was Sync but it needs to send an update
                    if device.isCheckedOut {
                        
                        let parameters: [String: Any] = [
                            "lastCheckedOutDate": TimestampHelper.getCurrentTimestamp(timestamp: device.lastCheckedOutDate!),
                            "lastCheckedOutBy": device.lastCheckedOutBy ?? "",
                            "isCheckedOut": true
                        ]
                        let url  = "\(Constants.BaseURLs.baseUrl)\(Constants.DeviceURLs.deviceEndpoint)/\(device.id)"
                        
                        NetworkRequestService.postDevice(url: url, parameters: parameters) {
                            (result: Any) in
                            print("device \(String(describing: result))")
                            //TODO update the item as Sync
                            
                        }
                    }
                }
              }
            }
            
            /*if reachability.isReachableViaWiFi
            {
                print("Reachable via WiFi")
            }*/
            
        }
        else
        {
            print("Network not reachable")
        }
    }

    // MARK: - Navigation
    
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "devicesDetails" {
            
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            navigationItem.backBarButtonItem = backItem
            
            let destViewController = segue.destination as! DeviceDetailsViewController
            
            // Pass the selected object to the new view controller.
            if let indexPath = self.deviceTableView.indexPathForSelectedRow {
                let selectedDevice = deviceListData[indexPath.row]
                destViewController.dataDetails = selectedDevice
            }
        }
        
    }


}
