#Manage Device #


### What is this repository for? ###

* Device manager app that allows to user manage devices, (Technical Challenge).
* Version 1.0.0


### How do I get set up? ###

* Summary of set up 
  Download the source code and remove pod folder and type in the terminal
   pod install 

### Patters and some swift concepts ###

* MVC patten
* Independent storyboards to avoid breakable when merging in Git
* Extensions Results from RealmSwift and UIApplication to detect first lunch 
* Reusable AlertViewControllerDelegate
* GCD to Sync devices
Swift 3.0
Deployment Target 10.3


### Contribution Third part libraries ###

###Realm Database###
https://cocoapods.org/pods/Realm
pros
Realm is an open source client side database for Mobile platforms. Read more on their web page.
Realm is great at this and it allows you to handle a lot of data fast.
Its quite easy to use and less complicated as compared to Core Data.

cons
To get started with Realm, all you need is at least iOS 8 or OS X 10.9. Older versions don’t support this new straightforward solution for managing local storage and databases.

### Alamofire ###
https://github.com/Alamofire/Alamofire
Alamofire is a Swift-based HTTP networking library for iOS
Why Alamofire? Alamofire access data on the Internet with very little effort, and your code will be much cleaner and easier to read.


### Reachability ###
https://github.com/ashleymills/Reachability.swift

This is a drop-in replacement for Apple's Reachability class. It is ARC-compatible, and it uses the new GCD methods to notify of network interface changes.

In addition to the standard NSNotification, it supports the use of blocks for when the network becomes reachable and unreachable. 

### Notes:###
Additional functionality to reset the devices list from the home page removing database and keep the devices that are created by the user.


### Who do I talk to? ###

* Repo Janet Rivas